import {Router} from 'express';
import getAuth from './get'
import login from './login'

import middAuth from '@middlewares/auth';
import middValidator from '@middlewares/data.validator';
import { check } from 'express-validator';
import messages from '@config/message';

const router= Router();
router.get('/', [middAuth.isAutenticated] ,getAuth)

router.post('/', [
  check('email').isEmail().withMessage(messages.ERROR_VALIDATION_EMAIL),
  check('password').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  middValidator.check
],login)


export default router;