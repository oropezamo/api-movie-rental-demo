import { Request, Response, NextFunction } from "express-serve-static-core";
import userService from '@services/user'
export default async function(req: Request, res: Response, next: NextFunction) {
  try {
    return res.json(await userService.getUsers())
  } catch (error) {
    next(error)
  }
}