import {Router} from 'express';
import listUsers from './list'
import get from './get'
import create from './create'
import update from './update'
import activate from './s.activate'
import inactivate from './s.inactivate'
import erase from './s.erase'

import middAuth from '@middlewares/auth';
import middValidator from '@middlewares/data.validator';
import { check } from 'express-validator';
import messages from '@config/message';

import _ from 'lodash'
import { ROLES_OPTIONS, STATUS_OPTIONS } from '@models/user';

const router= Router();


const roles = _.values(ROLES_OPTIONS)
const status = _.values(STATUS_OPTIONS)

router.use([
  middAuth.isAutenticated,
  middAuth.isAdmin
])

router.get('/',listUsers)

router.get('/:id', [
  check('id').isMongoId().withMessage(messages.ERROR_VALIDATION_ID),
  middValidator.check
], get)

router.post('/', [
  check('email').isEmail().withMessage(messages.ERROR_VALIDATION_EMAIL),
  check('password').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('firstName').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('lastName').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('address').optional().isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('phone').optional().isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('status').isIn(status).withMessage(messages.ERROR_VALIDATION_OPTION),
  check('role').isIn(roles).withMessage(messages.ERROR_VALIDATION_OPTION),
  middValidator.check
], create)


router.put('/:id', [
  check('id').isMongoId().withMessage(messages.ERROR_VALIDATION_ID),
  check('email').isEmail().withMessage(messages.ERROR_VALIDATION_EMAIL),
  check('password').optional().isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('firstName').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('lastName').isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('address').optional().isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('phone').optional().isLength({min: 4}).withMessage(messages.ERROR_VALIDATION_4_MINLENGTH),
  check('status').isIn(status).withMessage(messages.ERROR_VALIDATION_OPTION),
  check('role').isIn(roles).withMessage(messages.ERROR_VALIDATION_OPTION),
  middValidator.check
], update)

router.put('/:id/activate', [
  check('id').isMongoId().withMessage(messages.ERROR_VALIDATION_ID),
  middValidator.check
], activate)


router.put('/:id/inactivate', [
  check('id').isMongoId().withMessage(messages.ERROR_VALIDATION_ID),
  middValidator.check
], inactivate)


router.delete('/:id', [
  check('id').isMongoId().withMessage(messages.ERROR_VALIDATION_ID),
  middValidator.check
], erase)

export default router;