import { Request, Response, NextFunction } from "express-serve-static-core";
import userService from '@services/user'
export default async function(req: Request, res: Response, next: NextFunction) {
  try {
    let {firstName, lastName, email, password, status, role, address, phone} = req.body;
    return res.json(await userService.createUser({
      firstName,
      lastName,
      email,
      password,
      status,
      role,
      address,
      phone
    }))
  } catch (error) {
    next(error)
  }
}