import { Request, Response, NextFunction } from "express-serve-static-core";
import userService from '@services/user'
export default async function(req: Request, res: Response, next: NextFunction) {
  try {
    let {id} = req.params;
    return res.json(await userService.getUserById(id))
  } catch (error) {
    next(error)
  }
}