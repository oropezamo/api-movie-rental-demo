import {User, IUserModel, STATUS_OPTIONS, IUser, ROLES_OPTIONS} from '@models/user'
import IUserBase from '@interfaces/user';
import Secure from '@core/secure'
import _ from 'lodash';
import logger from '@core/logger';
import messages from '@config/message';
class UserService {
  constructor (private user:IUserModel = User) {}

  async _getUserById (id:string):Promise<IUser> {
    return this.user.findOne({_id: id, status: {$ne: STATUS_OPTIONS.ERASED}});
  }

  async _getUserByIdOrFail (id:string):Promise<IUser> {
    let user = await this._getUserById(id);
    if(user === null) {
      throw new Error(messages.ERROR_USER_NOT_EXIST)
    }
    return user;
  }

  async _getUserByEmail (email: string):Promise<IUser> {
    return this.user.findOne({email: email, status: {$ne: STATUS_OPTIONS.ERASED}});
  }

  private getPublicData (user:IUser):IUserBase {
    return _.omit(user.toObject(), ['password']) as IUserBase;
  }

  async getUsers ():Promise<IUserBase[]> {
    let users = await this.user.find({status: {$ne: STATUS_OPTIONS.ERASED}});
    return _.map(users, user => this.getPublicData(user))
  }

  async getUserById(id:string):Promise<IUserBase> {
    let user = await this._getUserByIdOrFail(id);
    return this.getPublicData(user)
  }

  async createUser (dataUser: IUserBase):Promise<IUserBase> {
    if (await this._getUserByEmail (dataUser.email) !== null) {
      throw new Error('Email exist already!')
    }
    let user = new this.user(dataUser);
    user.password = await Secure.hashText(dataUser.password);
    await user.save()
    return this.getPublicData (user);

  }

  async updateUser (id:string, dataUser: IUserBase):Promise<IUserBase> {
    let user = await this._getUserByIdOrFail(id)
    if(dataUser.password !== undefined) {
      user.password = await Secure.hashText(dataUser.password);
    }
    if(dataUser.address!== undefined) {
      user.address = dataUser.address
    }
    if(dataUser.phone !== undefined) {
      user.phone = dataUser.phone
    }
    user.role = dataUser.role
    user.status = dataUser.status
    await user.save()
    return this.getPublicData(user);
  }

  private async changeStatus (id:string, status: string):Promise<IUserBase> {
    let user = await this._getUserByIdOrFail(id)
    user.status = status;
    await user.save();
    return this.getPublicData(user);
  }

  async activateUser (id:string):Promise<IUserBase> {
    return this.changeStatus(id, STATUS_OPTIONS.ACTIVE)
  }

  async inactivateUser (id:string):Promise<IUserBase> {
    return this.changeStatus(id, STATUS_OPTIONS.INACTIVE)
  }

  async eraseUser (id:string):Promise<IUserBase> {
    return this.changeStatus(id, STATUS_OPTIONS.ERASED)
  }

  async createUserAdminDefault (email:string, password:string, firstName:string, lastName: string): Promise<boolean> {
    try {
      if(await this._getUserByEmail(email) !== null) {
        return true;
      }
      let dataUser = {
        email,
        password,
        firstName,
        lastName,
        status: STATUS_OPTIONS.ACTIVE,
        role: ROLES_OPTIONS.ADMIN
      }
      await this.createUser(dataUser);

      return true;
    } catch (error) {
      console.log(error)
      logger.log(messages.ERROR_CREATE_IMPORTANT_REGISTER, 'error')
      throw error
    }

  }

  async autenticateUser (email:string, password:string):Promise<string> {
    let user = await this._getUserByEmail (email);
    if(user === null) {
      throw new Error(messages.ERROR_EMAIL_PASSWORD_INCORRECT)
    }
    if(await Secure.compareHash(password, user.password) === false) {
      throw new Error(messages.ERROR_EMAIL_PASSWORD_INCORRECT)
    }
    if(user.status === STATUS_OPTIONS.INACTIVE) {
      throw new Error(messages.ERROR_USER_INACTIVE)
    }
    return Secure.JWT.createToken(this.getPublicData(user))
  }

  async getAutenticated (token:string):Promise<IUserBase> {
    let userData: IUserBase = await Secure.JWT.decodeToken(token);
    let user = await this._getUserByEmail(userData.email);
    if(user.status === STATUS_OPTIONS.INACTIVE) {
      throw new Error(messages.ERROR_USER_INACTIVE)
    }

    return this.getPublicData(user);
  }

  isAdmin (user:IUserBase) {
    return user.role === ROLES_OPTIONS.ADMIN
  }

  isVendor (user:IUserBase) {
    return user.role === ROLES_OPTIONS.VENDOR
  }


}

export default new UserService ();