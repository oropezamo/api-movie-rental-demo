export default interface IUserBase {
  firstName: string;
  lastName: string;
  email:string;
  phone?: string;
  address?: string
  password?:string;
  status: string;
  role: string;
}