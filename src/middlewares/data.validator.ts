import {Response, Request, NextFunction } from 'express-serve-static-core';
import { validationResult } from 'express-validator';
import messages from '@config/message';

class DataValidatorMiddleware {
  check (req:Request, res: Response, next: NextFunction) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        res.status(422).json({message: messages.ERROR_DATA_VALIDATION, errors: errors.array() });
        return;
      }
      next();
    } catch (error) {
      res.status(401)
      next(error)
    }
  }

}

export default new DataValidatorMiddleware ();