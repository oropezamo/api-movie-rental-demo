import UserService from '@services/user'
import {Response, Request, NextFunction } from 'express-serve-static-core';
import messages from '@config/message';

class AuthGuardMiddleware {
  async isAutenticated (req:Request, res: Response, next: NextFunction) {
    try {
      let headersAuth = req.headers.authorization;
      if (!headersAuth) {
        throw new Error(messages.ERROR_INCORRECT_CREDS);
      }
      let token:string = headersAuth.split(" ")[1];
      req.user = await UserService.getAutenticated(token);
      next();
    } catch (error) {
      res.status(401)
      next(error)
    }
  }

  async isAdmin (req:Request, res: Response, next: NextFunction) {
    try {
      if (!req.user) {
        throw new Error (messages.ERROR_FORBIDDEN)
      }
      if(UserService.isAdmin(req.user) === false) {
        throw new Error(messages.ERROR_FORBIDDEN)
      }
      next();
    } catch (error) {
      res.status(401)
      next(error)
    }
  }


  

}

export default new AuthGuardMiddleware ();