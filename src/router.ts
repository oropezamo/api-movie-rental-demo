import express from 'express';
import demoRoutes from '@modules/demo/demo.routes'
import authRoutes from '@modules/auth/auth.routes'
import userRoutes from '@modules/user/user.routes'
export default (app: express.Application) => {
  const routes = [
    {
      path: '/demo',
      module: demoRoutes
    },
    {
      path: '/auth',
      module: authRoutes
    },
    {
      path: '/users',
      module: userRoutes
    },
  ]

  //register routes on app
  routes.map(route => app.use(route.path, route.module))

}