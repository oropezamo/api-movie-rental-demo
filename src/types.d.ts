import IUserBase from './interfaces/user'
declare module 'express-serve-static-core' {
  interface Request {
    user?: IUserBase,
  }
}