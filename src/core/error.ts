import {Response, Request, NextFunction } from 'express-serve-static-core';

import messages from '@config/message'
import Logger from '@core/logger'
class ErrorHandleHelper {

  error404(req:Request, res:Response, next:NextFunction) {
    let err = new Error(messages.ERROR_NOT_FOUND);
    res.status(404)
    next(err);
  }

  errorGeneral(err:Error, req:Request, res:Response, next:NextFunction) {
    if(res.statusCode === 200) {
      res.status(422);
    }

    try {
      if (err.message != undefined && err.message != null){
        Logger.log(err.message, 'error')
        res.json({ success: false, message: err.message });
      } else {
        res.json({ success: false, message: messages.ERROR_INTERNAL_SYSTEM })
      }
    } catch (e) {
      Logger.log(e.message || messages.ERROR_INTERNAL_SYSTEM, 'error')
      res.json({ success: false, message: messages.ERROR_INTERNAL_SYSTEM });
    }
  }
}

export default new ErrorHandleHelper();