import constants from '@config/constants'
import * as jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt';
const JT_PASS = constants.SECURE.JT_PASS
const BCRYPT_SALT= parseInt(constants.SECURE.BCRYPT_SALT);
const crypto = require('crypto');

const decodeToken = async (token: string): Promise<any> =>{
  return new Promise((resolve, reject) => {
    jwt.verify(token, JT_PASS, function (err, decoded) {
      if (err) {
        return reject(err)
      }
      return resolve(decoded as object);
    });
  })
}

const createToken = async (data: object): Promise<string> => {
  return new Promise((resolve, reject) => {
    jwt.sign(data, JT_PASS, { algorithm: 'HS256' }, function (err, token) {
      if (err) {
        return reject(err)
      }
      return resolve(token);
    });
  })
}

const hashText = async (text:string):Promise<string> => {
  try {
    let salto = await generateSalt(BCRYPT_SALT);
    let hash = await getHash(salto, text);
    return hash;
  } catch (e) {
    throw ({ messageApp: e.messageApp, code: e.code, messageSystem: e });
  }
}

const compareHash = async (text:string, hash:string):Promise<boolean>  => {
  return bcrypt.compare(text, hash);
}

const generateSalt = async (saltos:number):Promise<string> => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(saltos, function (err, salt) {
      if (err) {
        return reject({ code: 'ERR_GEN', messageApp: 'ERROR_SISTEMA', messageSystem: err });
      }
      return resolve(salt);
    });
  });
}

const getHash = (salto:string, text:string):Promise<string> => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(text, salto, function (err, hash) {
      if (err) {
        return reject({ code: 'ERR_GEN', messageApp: 'ERROR_SISTEMA', messageSystem: err });
      }
      return resolve(hash);

    });
  });
}

const hash256 = (text:string):string => {
  return crypto.createHash('sha256').update(text).digest('hex');
}
const JWT = {
  decodeToken,
  createToken
}
export = {
  hash256,
  compareHash,
  hashText,
  JWT
}