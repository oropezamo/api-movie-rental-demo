import _ from 'lodash'
import {Document, Schema, Model, model} from 'mongoose'
import IUserBase from '@interfaces/user'

export interface IUserDocument extends Document, IUserBase {}

export interface IUser extends IUserDocument {}

export interface IUserModel extends Model<IUser> {}

export const STATUS_OPTIONS = {
  ACTIVE: 'active', 
  INACTIVE: 'inactive',
  ERASED: 'erased'
}

export const ROLES_OPTIONS = {
  ADMIN: 'admin',
  VENDOR: 'vendor',
  MEMBER: 'member'
}

const schema:Schema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  address: {
    type: String,
  },
  phone: {
    type: String,
  },
  password: {
    type: String,
    required: true
  },
  status: {
    type: String,
    enum: _.values(STATUS_OPTIONS),
    default: STATUS_OPTIONS.ACTIVE
  },
  role: {
    type: String,
    enum: _.values(ROLES_OPTIONS),
    default: ROLES_OPTIONS.ADMIN
  }
}, {timestamps: true})

export const User: IUserModel = model<IUser, IUserModel> ('user', schema, 'users')

