const constants = {
  APP: {
    PORT: process.env.PORT || '3000'
  },
  SECURE: {
    JT_PASS: process.env.JT_PASS,
    BCRYPT_SALT: process.env.BCRYPT_SALT
  },
  USER_ADMIN: {
    ADM_EMAIL: process.env.ADM_EMAIL,
    ADM_PASSWORD: process.env.ADM_PASSWORD,
    ADM_FIRSTNAME: process.env.ADM_FIRSTNAME,
    ADM_LASTNAME: process.env.ADM_LASTNAME
  },
  MONGO: {
    URI: process.env.MONGO_URI
  }

}

export default constants;