const messages = {
  'ERROR_INTERNAL_SYSTEM': 'Error internal system',
  'ERROR_NOT_FOUND': 'Error not found',
  'ERROR_CREATE_IMPORTANT_REGISTER': 'Error to create important register',

  'ERROR_EMAIL_PASSWORD_INCORRECT': 'Error email or password incorrect',
  'ERROR_USER_INACTIVE': 'Error user inactive',
  'ERROR_USER_NOT_EXIST': 'Error user not exist',

  'ERROR_INCORRECT_CREDS': 'Error incorrect creds',
  'ERROR_FORBIDDEN': 'Error forbidden',

  'ERROR_DATA_VALIDATION': 'Error in data validation',


  'ERROR_VALIDATION_EMAIL': 'The field must be a valid email field',
  'ERROR_VALIDATION_4_MINLENGTH': 'The field must be have at least 4 characters',
  'ERROR_VALIDATION_ID': 'Error id not valid',
  'ERROR_VALIDATION_OPTION': 'Error option not valid'
}
export default messages;