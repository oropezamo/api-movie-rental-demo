require('module-alias/register');
import * as dotenv from 'dotenv';
dotenv.config();
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import { Application } from 'express-serve-static-core';
import mongoose from 'mongoose';

import app from './app'
import router from './router'
import errorHandleHelper from '@core/error'
import constanst from '@config/constants'

import UserService from '@services/user'
import constants from '@config/constants';

const configDB = async () => {
  await mongoose.connect(constants.MONGO.URI, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
  const {ADM_EMAIL, ADM_FIRSTNAME, ADM_LASTNAME, ADM_PASSWORD} = constants.USER_ADMIN
  await UserService.createUserAdminDefault(ADM_EMAIL, ADM_PASSWORD, ADM_FIRSTNAME, ADM_LASTNAME)

  return true;
}

const configApp = async (app: Application) => {
  try {
    //config app
    app.use(helmet());
    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    //use router
    router(app);


    //use errors
    app.use(errorHandleHelper.error404);
    app.use(errorHandleHelper.errorGeneral);

    await configDB();
    return true
  } catch (error) {
    throw error;
  }
}

const bootApp = async (app: Application) => {
  try {
    await configApp(app);
    const port: string = constanst.APP.PORT
    app.listen(port)
  } catch (error) {
    throw error;
  }
}

bootApp(app)
.then(()=> console.log('app runing'))
.catch(e => console.log(e))

